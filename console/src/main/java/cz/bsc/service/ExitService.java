package cz.bsc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ExitService {

    private ConfigurableApplicationContext configurableApplicationContext;

    @Autowired
    public ExitService(ConfigurableApplicationContext configurableApplicationContext) {
        this.configurableApplicationContext = configurableApplicationContext;
    }

    public void exitApplication() {
        configurableApplicationContext.close();
    }
}
