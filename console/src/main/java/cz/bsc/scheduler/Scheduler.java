package cz.bsc.scheduler;

import cz.bsc.exception.NoSuchRateException;
import cz.bsc.model.Currency;
import cz.bsc.model.Payment;
import cz.bsc.service.BalanceService;
import cz.bsc.service.CurrencyConverterService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.Console;
import java.math.BigDecimal;

@Component
public class Scheduler {
    private static final Logger LOGGER = LogManager.getLogger();

    private BalanceService balanceService;
    private CurrencyConverterService currencyConverterService;
    private Console console;

    @Autowired
    public Scheduler(BalanceService balanceService, CurrencyConverterService currencyConverterService) {
        this.balanceService = balanceService;
        this.currencyConverterService = currencyConverterService;
        this.console = System.console();
    }

    @Scheduled(fixedRateString = "${output-scheduler.delay}")
    public void reportCurrentBalances() {
        LOGGER.info("Generating payments report.");
        console.printf("----------------\nReport:\n");
        printPayments();
        console.printf("----------------\n");
    }

    private void printPayments() {
        balanceService.getCurrentBalances().forEach(b ->
        {
            console.printf("%s %s", b.getCurrency().getSymbol(), b.getAmount());
            if (!b.getCurrency().equals(Currency.DOLLAR)) {
                appendDollarExchangeRate(b);
            } else {
                console.printf("\n");
            }
        });
    }

    private void appendDollarExchangeRate(Payment b) {
        final BigDecimal dollarAmountForPayment;
        try {
            dollarAmountForPayment = currencyConverterService.getDollarAmountForPayment(b);
            console.printf(" (USD %f)\n", dollarAmountForPayment);
        } catch (NoSuchRateException e) {
            console.printf(" (USD rate not available)\n");
        }
    }
}
