package cz.bsc.scheduler;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("output-scheduler")
@Getter
@Setter
public class SchedulerConfiguration {
    private String delayMillis;
}
