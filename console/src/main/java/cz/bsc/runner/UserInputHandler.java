package cz.bsc.runner;

import cz.bsc.service.ExitService;
import cz.bsc.service.PaymentStorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Scanner;

import static java.util.Objects.nonNull;

@Order(1)
@Component
public class UserInputHandler implements CommandLineRunner {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String QUIT_COMMAND = "quit";

    private PaymentStorageService paymentStorageService;
    private ExitService exitService;

    @Autowired
    public UserInputHandler(PaymentStorageService paymentStorageService, ExitService exitService) {
        this.paymentStorageService = paymentStorageService;
        this.exitService = exitService;
    }

    @Override
    public void run(String... args) throws Exception {
        // only run when console is available
        if (nonNull(System.console())) {
            scanUserInput();
        }
    }

    private void scanUserInput() throws InterruptedException {
        LOGGER.info("Scanning user inputs...");
        System.out.printf("Type '%s' to exit application or input payments in following format: 'AUD (-)12345'\n", QUIT_COMMAND);

        Scanner scanner = new Scanner(System.in);
        while(true) {
            if (scanner.hasNextLine()) {
                String userInput = scanner.nextLine();
                if (userInput.equals(QUIT_COMMAND)) {
                    exitService.exitApplication();
                } else {
                    boolean storedSuccessfully = paymentStorageService.importSinglePaymentFromString(userInput);
                    if (!storedSuccessfully) {
                        System.out.printf("Invalid payment representation: '%s'\n", userInput);
                    }
                }
            } else {
                Thread.sleep(1000L); // wait for user input
            }
        }
    }
}
