package cz.bsc.runner;

import cz.bsc.exception.InvalidSymbolException;
import cz.bsc.exception.PaymentFormatException;
import cz.bsc.model.Payment;
import cz.bsc.service.PaymentStorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

@Order(Ordered.HIGHEST_PRECEDENCE)
@Component
public class PaymentPreloader implements CommandLineRunner {

    private static final Logger LOGGER = LogManager.getLogger();

    private PaymentStorageService paymentStorageService;

    @Autowired
    public PaymentPreloader(PaymentStorageService paymentStorageService) {
        this.paymentStorageService = paymentStorageService;
    }

    @Override
    public void run(String... args) throws Exception {
        if (args.length > 0) {
            String filePath = args[0];
            if (isValidPath(filePath)) {
                readPaymentsFromFile(filePath);
            } else {
                LOGGER.warn("Invalid file path provided or file does not exist.");
            }

            if (args.length > 1) {
                LOGGER.info("Ignoring additional arguments.");
            }
        } else {
            LOGGER.debug("No file provided.");
        }
    }

    private void readPaymentsFromFile(String filePath) throws FileNotFoundException {
        LOGGER.info("Pre-loading payments from file: {}", filePath);
        File file = Paths.get(filePath).toFile();
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            paymentStorageService.importSinglePaymentFromString(line);
        }
    }

    private static boolean isValidPath(String path) {
        Path filePath;
        try {
            filePath = Paths.get(path);
        } catch (InvalidPathException | NullPointerException ex) {
            return false;
        }
        return filePath.toFile().exists();
    }
}
