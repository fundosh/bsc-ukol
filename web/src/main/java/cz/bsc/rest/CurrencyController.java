package cz.bsc.rest;

import cz.bsc.model.Currency;
import cz.bsc.service.CurrencyConverterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping("api/v1/currency")
public class CurrencyController {

    private CurrencyConverterService currencyConverterService;

    @Autowired
    public CurrencyController(CurrencyConverterService currencyConverterService) {
        this.currencyConverterService = currencyConverterService;
    }

    @GetMapping("usd-rate")
    public BigDecimal getConversionRate(@RequestParam String currencySymbol) throws Exception {
        return currencyConverterService.getDollarRateForCurrency(Currency.of(currencySymbol));
    }

}
