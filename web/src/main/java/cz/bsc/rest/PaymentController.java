package cz.bsc.rest;

import cz.bsc.model.Payment;
import cz.bsc.service.CurrencyConverterService;
import cz.bsc.service.PaymentStorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/payment")
public class PaymentController {
    private static final Logger LOGGER = LogManager.getLogger();

    private PaymentStorageService paymentStorageService;

    @Autowired
    public PaymentController(PaymentStorageService paymentStorageService) {
        this.paymentStorageService = paymentStorageService;
    }

    @PostMapping(consumes = {"application/json"})
    public void insertPayment(@RequestBody Payment payment) {
        LOGGER.info("Payment received: {}", payment);
        paymentStorageService.storePayment(payment);
    }

}
