package cz.bsc.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;

@Component
@ConfigurationProperties("converter")
@Getter
@Setter
public class ConverterConfiguration {
    private Map<String, BigDecimal> usdRates;
}
