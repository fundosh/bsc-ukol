package cz.bsc.service;

import cz.bsc.config.ConverterConfiguration;
import cz.bsc.exception.NoSuchRateException;
import cz.bsc.model.Currency;
import cz.bsc.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;

@Component
public class CurrencyConverterService {

    private ConverterConfiguration converterConfiguration;

    @Autowired
    public CurrencyConverterService(ConverterConfiguration converterConfiguration) {
        this.converterConfiguration = converterConfiguration;
    }

    public BigDecimal getDollarRateForCurrency(Currency currency) throws NoSuchRateException {
        if (!converterConfiguration.getUsdRates().containsKey(currency.getSymbol())) {
            throw new NoSuchRateException();
        }
        return converterConfiguration.getUsdRates().get(currency.getSymbol());
    }

    public BigDecimal getDollarAmountForPayment(Payment payment) throws NoSuchRateException {
        return getDollarRateForCurrency(payment.getCurrency())
                .multiply(valueOf(payment.getAmount()));
    }
}
