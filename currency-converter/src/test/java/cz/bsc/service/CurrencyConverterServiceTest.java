package cz.bsc.service;

import cz.bsc.exception.NoSuchRateException;
import cz.bsc.model.Currency;
import cz.bsc.model.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static cz.bsc.model.Currency.of;
import static cz.bsc.model.Payment.fromString;
import static java.math.BigDecimal.valueOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class CurrencyConverterServiceTest {

    @Autowired
    private CurrencyConverterService currencyConverterService;

    @Test
    void getDollarRateForCurrency() throws Exception {
        assertEquals(valueOf(1.5), currencyConverterService.getDollarRateForCurrency(of("EUR")));
        assertEquals(valueOf(0.75), currencyConverterService.getDollarRateForCurrency(of("GBP")));
    }

    @Test
    void getDollarAmountForPayment() throws Exception {
        assertEquals(valueOf(3.0), currencyConverterService.getDollarAmountForPayment(fromString("EUR 2")));
        assertEquals(valueOf(2.25), currencyConverterService.getDollarAmountForPayment(fromString("GBP 3")));
    }

    @Test
    void noRateAvailable() {
        assertThrows(NoSuchRateException.class, () -> currencyConverterService.getDollarRateForCurrency(of("XYZ")));
        assertThrows(NoSuchRateException.class, () -> currencyConverterService.getDollarAmountForPayment(fromString("XYZ 2")));
    }
}