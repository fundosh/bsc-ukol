package cz.bsc;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@SpringBootConfiguration
@EnableAutoConfiguration
public class CurrencyConverterTestConfiguration extends CurrencyConverterConfiguration {
}
