package cz.bsc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = "cz.bsc")
@EnableScheduling
public class PaymentTrackerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaymentTrackerApplication.class, args);
    }

}
