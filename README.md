# README #

Project implements requirements given to the candidate via email.

### How to run this project ###
<sub><sub>Use forward slashes when not using Windows. Add execute permission for [gradlew script](https://docs.gradle.org/current/userguide/gradle_wrapper.html)</sub></sub>

Build:

```.\gradlew build```

Run application (path to file is optional):

```java -jar .\build\libs\PaymentTracker-1.0-SNAPSHOT.jar path\to\payments_file'```

### Assumptions and notes ###

* JDK11
* currency code can only be 3 letters
* payment value is long only
* sample currency rates are available for EUR, GBP, JPY, CZK, CHF, CNY
* 'quit' command needs second return to actually work
* there is a rest endpoint to add payments
* payments can be entered simultaneously
* balance recalculation is synchronized
* user input errors are handled gracefully - error is printed and logged but app continues

### TODO ###

* fix 'quit' command
* Preemptively drain the payment queue when certain capacity of queue is reached.