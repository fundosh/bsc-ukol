package cz.bsc.model;

import cz.bsc.exception.InvalidSymbolException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

public class CurrencyTest {

    @Test
    public void currencySymbolCanBe3LettersOnly() {
        assertThrows(InvalidSymbolException.class, () -> Currency.of("A"));
        assertThrows(InvalidSymbolException.class, () -> Currency.of(null));
        assertThrows(InvalidSymbolException.class, () -> Currency.of("123"));
        assertThrows(InvalidSymbolException.class, () -> Currency.of("ABCD"));
        assertThrows(InvalidSymbolException.class, () -> Currency.of("A98"));
    }
}