package cz.bsc.model;

import cz.bsc.exception.InvalidSymbolException;
import cz.bsc.exception.PaymentFormatException;
import org.junit.jupiter.api.Test;

import static cz.bsc.model.Payment.fromString;
import static org.junit.jupiter.api.Assertions.*;

public class PaymentTest {

    @Test
    public void canCreatePaymentFromString() throws Exception {
        Payment usd542 = fromString("USD 542");
        assertAll(
                () -> assertEquals(Currency.of("USD"), usd542.getCurrency()),
                () -> assertEquals(542L, usd542.getAmount())
        );
    }

    @Test
    public void cannotCreatePaymentWithInvalidSymbol() {
        assertThrows(InvalidSymbolException.class, () -> fromString("A234 452"));
    }

    @Test
    public void cannotCreatePaymentWithInvalidNumber() {
        assertThrows(PaymentFormatException.class, () -> fromString("AUD 432.2"));
        assertThrows(PaymentFormatException.class, () -> fromString("AUD 432-2"));
        assertThrows(PaymentFormatException.class, () -> fromString("AUD *432-2"));
        assertThrows(PaymentFormatException.class, () -> fromString("AUD 432*"));
        assertThrows(PaymentFormatException.class, () -> fromString("AUD 999999999999999999999"));
        assertThrows(PaymentFormatException.class, () -> fromString("AUD asdf"));
    }
}