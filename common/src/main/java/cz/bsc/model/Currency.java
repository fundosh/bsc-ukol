package cz.bsc.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import cz.bsc.exception.InvalidSymbolException;
import lombok.Data;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static java.util.Objects.isNull;

@Data
public class Currency {
    private static final Logger LOGGER = LogManager.getLogger();
    static final int CURRENCY_SIZE = 3;
    private static final Pattern SYMBOL_PATTERN = Pattern.compile("[a-zA-Z]{" + CURRENCY_SIZE + "}");
    private static Map<String, Currency> currencies = new HashMap<>();
    public static final Currency DOLLAR = new Currency("USD");

    @Getter
    private String symbol;

    private Currency(String symbol) {
        this.symbol = symbol;
    }

    @JsonCreator
    public static Currency of(String symbol) throws InvalidSymbolException {
        if (isNull(symbol) || !currencies.containsKey(symbol)) {
            symbol = sanitizeSymbol(symbol);
        }
        return currencies.computeIfAbsent(symbol, Currency::new);
    }

    private static String sanitizeSymbol(String symbol) throws InvalidSymbolException {
        if (isNull(symbol) || !SYMBOL_PATTERN.matcher(symbol).matches()) {
            LOGGER.warn("Invalid currency symbol - {}", symbol);
            throw new InvalidSymbolException("Cannot parse currency symbol. Correct format is: 'AUD'");
        } else {
            symbol = symbol.toUpperCase();
        }
        return symbol;
    }
}
