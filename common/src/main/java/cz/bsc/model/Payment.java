package cz.bsc.model;

import cz.bsc.exception.InvalidSymbolException;
import cz.bsc.exception.PaymentFormatException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static cz.bsc.model.Currency.CURRENCY_SIZE;

@Data
@AllArgsConstructor
public class Payment {
    private static final Logger LOGGER = LogManager.getLogger();

    private Currency currency;
    private long amount;

    public static Payment fromString(String paymentDescription) throws InvalidSymbolException, PaymentFormatException {
        Currency c = Currency.of(paymentDescription.substring(0, CURRENCY_SIZE));
        String amount = paymentDescription.substring(CURRENCY_SIZE).strip();
        long amountValue;
        try {
            amountValue = Long.parseLong(amount);
        } catch (NumberFormatException e) {
            LOGGER.warn("Cannot parse payment amount - {}", amount);
            throw new PaymentFormatException("Cannot parse payment amount. Correct format is: 'AUD (-)12345'");
        }

        return new Payment(c, amountValue);
    }
}
