package cz.bsc.exception;

public class PaymentFormatException extends Exception {
    public PaymentFormatException(String s) {
        super(s);
    }
}
