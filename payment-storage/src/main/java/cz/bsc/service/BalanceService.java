package cz.bsc.service;

import cz.bsc.model.Currency;
import cz.bsc.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class BalanceService {

    private static final Predicate<? super Map.Entry<Currency, Long>> NON_ZERO_BALANCE = e ->
            nonNull(e.getValue()) && e.getValue() != 0L;

    private final Map<Currency, Long> currentBalances;
    private PaymentStorageService paymentStorageService;

    @Autowired
    public BalanceService(PaymentStorageService paymentStorageService) {
        this.paymentStorageService = paymentStorageService;
        this.currentBalances = new HashMap<>();
    }

    public Set<Payment> getCurrentBalances() {
        addPendingPaymentsToBalance();
        return currentBalances.entrySet().stream()
                .filter(NON_ZERO_BALANCE)
                .map(e -> new Payment(e.getKey(), e.getValue()))
                .collect(Collectors.toSet());
    }

    void addPendingPaymentsToBalance() {
        synchronized (currentBalances) {
            List<Payment> pendingPayments = paymentStorageService.getPendingPayments();
            pendingPayments.forEach(
                    p -> currentBalances.compute(p.getCurrency(), (currency, oldAmount) -> {
                        long total = p.getAmount();
                        if (nonNull(oldAmount)) {
                            total += oldAmount;
                        }
                        return total;
                    })
            );
        }
    }

}
