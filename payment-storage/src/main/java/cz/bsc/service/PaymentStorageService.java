package cz.bsc.service;

import cz.bsc.exception.InvalidSymbolException;
import cz.bsc.exception.PaymentFormatException;
import cz.bsc.model.Payment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Component
public class PaymentStorageService {

    private static final int CAPACITY = 1024;
    private static final Logger LOGGER = LogManager.getLogger();

    private BlockingQueue<Payment> pendingPayments;

    @Autowired
    public PaymentStorageService() {
        this.pendingPayments = new ArrayBlockingQueue<>(CAPACITY);
    }

    public void storePayment(Payment payment) {
        try {
            pendingPayments.put(payment);
        } catch (InterruptedException e) {
            LOGGER.error("Cannot store payment {}.", payment, e);
        }

        // TODO preemptively add to current balances
//        if (pendingPayments.size() > CAPACITY/2) {
//        }
    }

    public boolean importSinglePaymentFromString(String line) {
        Payment payment;
        try {
            payment = Payment.fromString(line);
        } catch (InvalidSymbolException | PaymentFormatException e) {
            return false;
        }
        storePayment(payment);
        return true;
    }

    List<Payment> getPendingPayments() {
        List<Payment> drainedPayments = new ArrayList<>();
        pendingPayments.drainTo(drainedPayments);
        return drainedPayments;
    }
}
