package cz.bsc.service;

import cz.bsc.model.Currency;
import cz.bsc.model.Payment;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PaymentStorageServiceTest {

    @Autowired
    private PaymentStorageService paymentStorageService;

    @Test
    public void storedPaymentsAreRetrieved() throws Exception {
        paymentStorageService.storePayment(new Payment(Currency.of("EUR"), 23L));
        paymentStorageService.storePayment(new Payment(Currency.of("GBP"), 12L));
        paymentStorageService.storePayment(new Payment(Currency.of("GBP"), 13L));
        paymentStorageService.storePayment(new Payment(Currency.of("CHF"), 3L));

        List<Payment> expectedPayments = new ArrayList<>();
        expectedPayments.add(new Payment(Currency.of("EUR"), 23L));
        expectedPayments.add(new Payment(Currency.of("GBP"), 12L));
        expectedPayments.add(new Payment(Currency.of("GBP"), 13L));
        expectedPayments.add(new Payment(Currency.of("CHF"), 3L));

        assertIterableEquals(expectedPayments, paymentStorageService.getPendingPayments());
    }

    @Test
    public void pendingPaymentsAreEmptyWhenDrained() throws Exception {
        paymentStorageService.storePayment(new Payment(Currency.of("EUR"), 23L));
        paymentStorageService.storePayment(new Payment(Currency.of("GBP"), 12L));
        paymentStorageService.storePayment(new Payment(Currency.of("CHF"), 3L));

        paymentStorageService.getPendingPayments();

        assertIterableEquals(Collections.emptyList(), paymentStorageService.getPendingPayments());
    }

}