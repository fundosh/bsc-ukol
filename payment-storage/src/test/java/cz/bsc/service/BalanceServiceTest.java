package cz.bsc.service;

import cz.bsc.model.Currency;
import cz.bsc.model.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class BalanceServiceTest {

    @Autowired
    private BalanceService balanceService;

    @Autowired PaymentStorageService paymentStorageService;

    @Test
    public void pendingPaymentsAreAdded() throws Exception {
        assertIterableEquals(Collections.emptySet(), balanceService.getCurrentBalances());
        List<Payment> pendingPayments = new ArrayList<>();
        paymentStorageService.storePayment(new Payment(Currency.of("EUR"), 23L));
        paymentStorageService.storePayment(new Payment(Currency.of("GBP"), 12L));
        paymentStorageService.storePayment(new Payment(Currency.of("JPY"), 1L));
        paymentStorageService.storePayment(new Payment(Currency.of("EUR"), 2L));
        paymentStorageService.storePayment(new Payment(Currency.of("GBP"), -25L));
        paymentStorageService.storePayment(new Payment(Currency.of("AUD"), -4L));
        paymentStorageService.storePayment(new Payment(Currency.of("JPY"), -1L));

        balanceService.addPendingPaymentsToBalance();

        Set<Payment> expectedPayments = new HashSet<>();
        expectedPayments.add(new Payment(Currency.of("EUR"), 25L));
        expectedPayments.add(new Payment(Currency.of("GBP"), -13L));
        expectedPayments.add(new Payment(Currency.of("AUD"), -4L));
        //missing JPY - it equals 0

        assertThat(balanceService.getCurrentBalances(), containsInAnyOrder(expectedPayments.toArray()));
    }

}